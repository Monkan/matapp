//
//  ViewController.m
//  FoodApp
//
//  Created by ITHS on 2016-03-03.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *searchBox;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)getSearchResults:(id)sender {
}


@end
