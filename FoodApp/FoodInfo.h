//
//  FoodInfo.h
//  FoodApp
//
//  Created by ITHS on 2016-03-03.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodInfo : NSObject

-(instancetype)initWithText:(NSString*) foodText;

@property (nonatomic)NSString *foodText;

@end
